package com.example.shibejava.domain;

import static org.junit.Assert.*;

import android.util.Log;

import androidx.lifecycle.LiveData;

import com.example.shibejava.data.ImageRepo;
import com.example.shibejava.data.local.dao.ImageDao;
import com.example.shibejava.data.local.dao.ImageDao_Impl;
import com.example.shibejava.data.local.entity.Image;
import com.example.shibejava.data.remote.ImageService;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.w3c.dom.Entity;

import java.util.List;

import javax.inject.Inject;

public class GetShibeImagesUseCaseTest {

    private GetShibeImagesUseCase getShibeImagesUseCase;
    private ImageService service;
    private ImageDao imageDao;
    private ImageRepo repo;




    public GetShibeImagesUseCaseTest(
//            ImageService service,
//            ImageDao dao,
//            ImageRepo repo,
//            GetShibeImagesUseCase getShibeImagesUseCase
    ) {
//        this.repo = repo;
//        this.getShibeImagesUseCase = getShibeImagesUseCase;
    }

@Before
public void SetUp(){

       repo = new ImageRepo(service, imageDao);

}

    @Test
    public void GIVEN_new_GetShibeImagesUseCase_instance_WHEN_invoked_THEN_return_success_result_with_shines() {
        //Given
       getShibeImagesUseCase = new GetShibeImagesUseCase(repo);
        //When
        List<Image>result =  getShibeImagesUseCase.getShibeImages(100).getValue();
       List<String>newList = result.stream().flatMap((each)->  {
         String url =  each.getUrl();
         return url.toString();
                   });
        //Then
        Assert.assertEquals(100, result.size());


    }
}