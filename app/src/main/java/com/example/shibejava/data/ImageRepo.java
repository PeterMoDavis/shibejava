package com.example.shibejava.data;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.example.shibejava.data.local.dao.ImageDao;
import com.example.shibejava.data.local.entity.Image;
import com.example.shibejava.data.remote.ImageService;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@Singleton
public class ImageRepo {
    private final ImageService imageService;
    private final ImageDao imageDao;
@Inject
    public ImageRepo(ImageService imageService, ImageDao imageDao){
        this.imageService = imageService;
        this.imageDao = imageDao;
    }

    public LiveData<List<Image>> getImages(int count) {
        List<Image>cachedList = imageDao.getAllImages().getValue();

        if(cachedList != null && !cachedList.isEmpty()) return imageDao.getAllImages();
        else return fetchImagesFromServerAndCache(count);
    }
    @NonNull
    private MutableLiveData<List<Image>> fetchImagesFromServerAndCache(int count) {
        MutableLiveData<List<Image>> imagesLD = new MutableLiveData<>();

        imageService.getShibeUrls(count).enqueue(new Callback<List<String>>() {
            @Override
            public void onResponse(
                    @NonNull Call<List<String>> call,
                    @NonNull Response<List<String>> response

            ) {
                if (response.isSuccessful() && response.body() != null) {
                    List<Image> imageUrls = new ArrayList<>();
                    for (String url : response.body()) {
                        imageUrls.add(new Image(url, Image.Type.SHIBE));
                    }
                    imagesLD.setValue(imageUrls);
                }

            }

            @Override
            public void onFailure(@NonNull Call<List<String>> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
        return imagesLD;
    }
}
