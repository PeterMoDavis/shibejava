package com.example.shibejava.data.local.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.shibejava.data.local.entity.Image;

import java.util.List;

@Dao
public interface ImageDao {
    @Insert
    void insert(Image image);

    @Update
    void update(Image image);

    @Delete
    void delete(Image image);

    @Query("DELETE FROM image")
    void deleteAllImages();

    @Query("SELECT * FROM image")
    LiveData<List<Image>> getAllImages();
}
