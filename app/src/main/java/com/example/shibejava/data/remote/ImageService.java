package com.example.shibejava.data.remote;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ImageService {
        @GET("/api/shibes")
        Call<List<String>> getShibeUrls(@Query("count") int count);
}
