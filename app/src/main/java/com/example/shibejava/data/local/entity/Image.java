package com.example.shibejava.data.local.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import kotlin.experimental.ExperimentalTypeInference;


@Entity
public class Image {


    @PrimaryKey(autoGenerate = true)
    private int id;
    private String url;
    private Type type;

    //constructor
    public Image(String url, Type type){
        this.type = type;
        this.url = url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public enum Type {SHIBE}
}
