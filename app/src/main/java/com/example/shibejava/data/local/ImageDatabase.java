package com.example.shibejava.data.local;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.shibejava.data.local.dao.ImageDao;
import com.example.shibejava.data.local.entity.Image;

@Database(entities = {Image.class}, version = 1, exportSchema = false)
public abstract class ImageDatabase extends RoomDatabase {
    private static ImageDatabase instance;
    public abstract ImageDao imageDao();


    public static synchronized ImageDatabase getInstance(Context context){
        if(instance == null){
            instance = Room.databaseBuilder(
                    context.getApplicationContext(), ImageDatabase.class, "image_database"
            ).build();
        }
        return instance;
    }
}
