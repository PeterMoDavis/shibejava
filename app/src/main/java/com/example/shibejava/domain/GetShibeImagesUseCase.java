package com.example.shibejava.domain;

import androidx.lifecycle.LiveData;

import com.example.shibejava.data.ImageRepo;
import com.example.shibejava.data.local.entity.Image;

import java.util.List;

import javax.inject.Inject;

public class GetShibeImagesUseCase {

    private final ImageRepo repo;

    @Inject
    public GetShibeImagesUseCase(ImageRepo repo) {
        this.repo = repo;
    }

    public LiveData<List<Image>> getShibeImages(int count){
        return repo.getImages(count);
    }
}
