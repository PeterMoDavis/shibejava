package com.example.shibejava.di;

import androidx.annotation.NonNull;

import com.example.shibejava.data.remote.ImageService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.components.SingletonComponent;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
@InstallIn(SingletonComponent.class)
public class RemoteModule {

    @Provides
    @Singleton
    Retrofit providesRetroFit() {
        return new Retrofit.Builder()
                .baseUrl("https://shibe.online")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    ImageService providesShibeService(@NonNull Retrofit retrofit) {
        return retrofit.create(ImageService.class);
    }


}
