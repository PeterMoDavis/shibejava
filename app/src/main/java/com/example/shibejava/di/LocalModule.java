package com.example.shibejava.di;

import android.content.Context;

import androidx.annotation.NonNull;

import com.example.shibejava.data.local.ImageDatabase;
import com.example.shibejava.data.local.dao.ImageDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import dagger.hilt.InstallIn;
import dagger.hilt.android.qualifiers.ApplicationContext;
import dagger.hilt.components.SingletonComponent;

@Module
@InstallIn(SingletonComponent.class)
public class LocalModule {

    @Provides
    @Singleton
    ImageDatabase providesImageDataBase(@ApplicationContext Context context) {
        return ImageDatabase.getInstance(context);
    }

    @Provides
    @Singleton
    ImageDao providesImageDao(@NonNull ImageDatabase imageDatabase) {
        return imageDatabase.imageDao();
    }


}
