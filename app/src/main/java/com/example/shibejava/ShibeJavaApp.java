package com.example.shibejava;

import android.app.Application;

import dagger.hilt.android.HiltAndroidApp;

@HiltAndroidApp
public class ShibeJavaApp extends Application { }
