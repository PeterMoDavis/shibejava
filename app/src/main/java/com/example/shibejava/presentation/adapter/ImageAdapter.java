package com.example.shibejava.presentation.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.shibejava.data.local.entity.Image;
import com.example.shibejava.databinding.ItemImageBinding;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.ImageViewHolder> {
    private final List<Image> images;

    public ImageAdapter() {
        this.images = new ArrayList<>();
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemImageBinding binding = ItemImageBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false
        );
        return new ImageViewHolder(binding);
    }

    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        holder.loadImage(images.get(position));
    }

    @Override
    public int getItemCount() {
        return images.size();
    }

    public void loadImage(List<Image> images) {
        int oldSize = this.images.size();
        this.images.clear();
        notifyItemRangeRemoved(0, oldSize);
        this.images.addAll(images);
        notifyItemRangeInserted(0, images.size());

    }

    static class ImageViewHolder extends RecyclerView.ViewHolder {
        private final ItemImageBinding binding;

        public ImageViewHolder(@NonNull ItemImageBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        void loadImage(Image image) {
            Picasso.get().load(image.getUrl()).into(binding.getRoot());
        }
    }
}
