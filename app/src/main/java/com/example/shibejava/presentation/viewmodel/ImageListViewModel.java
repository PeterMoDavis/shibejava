package com.example.shibejava.presentation.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

import com.example.shibejava.data.local.entity.Image;
import com.example.shibejava.domain.GetShibeImagesUseCase;

import java.util.List;

import javax.inject.Inject;

import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
public class ImageListViewModel extends ViewModel {
    private final GetShibeImagesUseCase getShibeImagesUseCase;

    @Inject
    public ImageListViewModel(GetShibeImagesUseCase getShibeImagesUseCase) {
        this.getShibeImagesUseCase = getShibeImagesUseCase;
    }

    public LiveData<List<Image>> getShibeImages(){
        return getShibeImagesUseCase.getShibeImages(100);
    }

}
