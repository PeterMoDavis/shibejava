package com.example.shibejava.presentation.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;


import com.example.shibejava.databinding.FragmentImageListBinding;
import com.example.shibejava.presentation.adapter.ImageAdapter;
import com.example.shibejava.presentation.viewmodel.ImageListViewModel;

import dagger.hilt.android.AndroidEntryPoint;

@AndroidEntryPoint
public class ImageListFragment extends Fragment {

   private FragmentImageListBinding binding;
   private ImageListViewModel imageListViewModel;
   private ImageAdapter imageAdapter;

   public ImageListFragment(){

   }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentImageListBinding.inflate(inflater, container, false);
        imageListViewModel = new ViewModelProvider(this).get(ImageListViewModel.class);
        imageAdapter = new ImageAdapter();
        binding.rvImageList.setAdapter(imageAdapter);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        imageListViewModel.getShibeImages().observe(
                getViewLifecycleOwner(), imageAdapter::loadImage
        );
    }

    @Override
    public void onDestroyView(){
       super.onDestroyView();
       binding = null;
    }
}
